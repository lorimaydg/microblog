<!DOCTYPE html>
<html>
<head>

	<title>MicroBlog</title>
	<link rel="stylesheet" href="css/myStyle.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">

</head>
<body>

	<div class="box_login">
		
		<text class="index_heading">MicroBlog</text>
		<br/><br/>

		<form method="POST" action="">
			<input class="input_field" type="text" name="username" id="username" placeholder="Username" /><br/>
			<label style="color:red; position:relative; left:90px; top: 5px;" id="error_user"></label><br/><br/>
			<input class="input_field" type="password" name="password" id="password" placeholder="Password" /><br/>
			<label style="color:red; position:relative; left:90px; top: 5px;" id="error_pass"></label><br/><br/>
			<br/>
			<input class="index_btn1" type="submit" name="submit" id="submit" value="LOGIN" />
		</form>

		<br/><br/>
		<text style="font-family: arial; font-size: 14px">No Account? <a href="dev/signup.php" style="color: blue">Sign up</a></text>
		<text style="font-family:arial; font-size:14px; float: right;"><a href="dev/forgot_password.php" style="color: blue">Forgot Password?</a></text>
		
	</div>

	<?php

	    session_start();

		include ('dev/db.php');

		$u_validation = 0;
	    $p_validation = 0;

	    if (isset($_POST['submit'])) {
	            
	        $user = $_POST['username'];
	        $pass = $_POST['password'];

	        $sql = "SELECT * FROM users";
	        $result = $con->query($sql);

	        while ($row = mysqli_fetch_assoc($result)) {
	        	if ($user==$row['username']) {
		    		$_SESSION['id_logged'] = $row['id'];
	        		$u_validation = 1;

	        		if ($row['password']==md5($pass)) {
			        	$p_validation = 1;
			        } else {
			        	$p_validation = 0;
			        }
	        	}
	        }

	        if ($user==null) {
	        	echo "
	        		<script type='text/javascript'>
		        		document.getElementById('error_user').innerHTML = '* Username required';
		        	</script>
	        	";
	        } else if ($u_validation==0) {
		        echo "
		        	<script type='text/javascript'>
		        		document.getElementById('error_user').innerHTML = '* Invalid Username';
		        	</script>
		        ";
		    }

		    if ($pass==null) {
	        	echo "
	        		<script type='text/javascript'>
		        		document.getElementById('error_pass').innerHTML = '* Password required';
		        	</script>
	        	";
	        } else if ($p_validation==0) {
		        echo "
		        	<script type='text/javascript'>
		        		document.getElementById('error_pass').innerHTML = '* Invalid Password';
		        	</script>
		        ";
		    }

		    if ($u_validation==1 && $p_validation==1) {
		    	header("Location: dev/home.php");
		    }
	    }

	    $con->close();

	?>

</body>
</html>