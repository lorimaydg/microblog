<!DOCTYPE html>
<html>
<head>

	<title>MicroBlog</title>
	<link rel="stylesheet" href="myStyle.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">

</head>
<body>

<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	if (isset($_SESSION['to_be_validated'])) {

		$myemail = 'microblog0001@gmail.com';
		$mypass = 'MicroBlogMailer0001';

		require 'vendor/autoload.php';

		$mail = new PHPMailer(true);
		try {
			//$m->SMTPDebug = 2;
			$mail->SetFrom($myemail);
			$mail->isSMTP();
		    $mail->SMTPAuth = true;
		    $mail->Host = 'smtp.gmail.com';
		    $mail->Username = $myemail;
		    $mail->Password = $mypass;
		    $mail->SMTPSecure = 'ssl';
		    $mail->Port = 465;
		    $mail->isHTML(true);
		    $mail->Subject = 'Email Validation';
		    $mail->Body = "Your account in MicroBlog has been successfully created/updated. <br/>Please click on the link to activate your account. <br/><br/>";
			$mail->Body .= "<a href='localhost/test/Microblog/dev/activate.php'>Activate Account</a>";
		    $mail->FromName = 'MicroBlog';
		    $mail->AddAddress($_SESSION['to_be_validated']);
	  
	  		$mail->send();
		    header("Location: /test/Microblog/dev/go_to_email.php");
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
	}

?>


<!---
	
	Google Account Information:

	Name: Micro Blog
	Email Address: microblog0001@gmail.com
	Password: MicroBlogMailer0001

---->


</body>
</html>