<!DOCTYPE html>
<html>
<head>

	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/myStyle.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">

</head>
<body>

	<div class="box_login">
		
		<div class="exit">
			<a href="../index.php"><button class="btn_exit">x</button></a>
		</div>

		<br/><br/>
		<text class="text_edit_pass">Forgot Password</text>
		<br/><br/>

		<form method="POST" action="">
			<input class="input_field" type="text" name="email" id="email" placeholder="Email Address" /><br/>
			<label style="color:red; position:relative; left:90px; top: 5px;" id="error_email"></label><br/>
			<input class="input_field" type="password" name="password" id="password" placeholder="New Password" /><br/><br/>
			<input class="input_field" type="password" name="password2" id="password2" placeholder="Re-Type New Password" /><br/>
			<label style="color:red; position:relative; left:90px; top: 5px;" id="error_pass"></label><br/><br/>
			<br/>
			<input class="confirm_btn" type="submit" name="submit" id="submit" value="CONFIRM" />
		</form>
		
	</div>

	<?php

	    session_start();

		include ('../dev/db.php');

		$e_validation = 0;
	    $new_p_validation = 0;

	    if (isset($_POST['submit'])) {
	            
	        $email = $_POST['email'];
	        $pass = $_POST['password'];
	        $pass2 = $_POST['password2'];

	        
		    $sql = "SELECT * FROM users";
	        $result = $con->query($sql);

	        $email_registered = 0;

	        while ($row = mysqli_fetch_assoc($result)) {
			    if ($row['email'] == $email) {
			        $email_registered = 1;
			    }
		    }

	        if ($email == null) {
	        	echo "
	        		<script type='text/javascript'>
		        		document.getElementById('error_email').innerHTML = '* Email Address required';
		        	</script>
	        	";
	        } else {
		        if ($email_registered==0) {
		        	echo "
		        		<script type='text/javascript'>
			        		document.getElementById('error_email').innerHTML = '* Email Address not registered';
			        	</script>
		        	";
		    	} else {
		        	$e_validation = 1;
		        }
	        }
	        
	        if ($pass == null) {
				echo "
	        		<script type='text/javascript'>
		        		document.getElementById('error_pass').innerHTML = '* Passwords required';
		        	</script>
	        	";
	        } else {
		        if ($pass == $pass2) {
		        	$new_p_validation = 1;
		        } else {
		        	echo "
		        		<script type='text/javascript'>
			        		document.getElementById('error_pass').innerHTML = '* Passwords do not match';
			        	</script>
		        	";
		        }
		    }

		    if ($pass2 == null) {
				echo "
	        		<script type='text/javascript'>
		        		document.getElementById('error_pass').innerHTML = '* Passwords required';
		        	</script>
	        	";
	        } else {
		        if ($pass == $pass2) {
		        	$new_p_validation = 1;
		        } else {
		        	echo "
		        		<script type='text/javascript'>
			        		document.getElementById('error_pass').innerHTML = '* Passwords do not match';
			        	</script>
		        	";
		        }
		    }


	        if ($e_validation == 1 && $new_p_validation == 1) {
	        	$_SESSION['email_to_send_confirmation'] = $email;
	        	$_SESSION['new_pass'] = $pass;
	        	header("Location: ../lib/PHPMailer/forgot_pass_email.php");
	        }

	    }

	    $con->close();

	?>

</body>
</html>