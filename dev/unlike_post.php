<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	include ('db.php');
	$id_logged = $_SESSION['id_logged'];

	$i = $_GET['i'];
	
	if ($i==1) {
		$page = $_GET['page'];
	}

	date_default_timezone_set('Asia/Manila');
	$current_date_time = date('Y-m-d G:i:s');


	if ($_POST['action'] == 'Unlike') {

		$uid = $_POST['uid'];
		$pid = $_POST['pid'];
		$sql = $con->prepare("DELETE FROM likes WHERE user_id=? AND post_id=?");
		$sql->bind_param("ii", $id_logged, $pid);		
		$sql->execute();
			
	}
	
	if ($_POST['action2'] == 'Unrepost') {
	    
		$uid = $_POST['uid2'];
		$pid = $_POST['pid2'];
		$sql = $con->prepare("DELETE FROM reposts WHERE user_id=? AND post_id=?");
		$sql->bind_param("ii", $id_logged, $pid);		
		$sql->execute();

	} 

	if ($_POST['action2'] == 'Repost') {
	    
		$uid = $_POST['uid2'];
		$pid = $_POST['pid2'];
	    $sql = $con->prepare("INSERT INTO reposts (user_id, post_id, date_reposted) VALUES (?,?,?)");
		$sql->bind_param("iis", $id_logged, $pid, $current_date_time);		
		$sql->execute();

	} 

	if ($i==1) {
		header("Location: home.php?page=$page");
	} else if ($i==2) {
		header("Location: view_user_profiles.php?id=$uid");
	} else if ($i==3) {
		header("Location: all_post.php");
	} else if ($i==4) {
		header("Location: view_post.php?p=$pid");
	} 

	$sql->close();
	$con->close();
	
?>