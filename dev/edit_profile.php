<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
	<script src="../lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
	<script src="../lib/JavaScript-MD5-master/js/md5.min.js"></script>
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar-disabled.php');

	?>

	<div class="main">

		<div align="center">
			<label class="edit_profile">EDIT PROFILE</label>
		</div>
		<br/><br/>


		<?php 

			if (isset($_POST['fn'])) {

				$fname_valid = $_GET['fn'];
				$lname_valid = $_GET['ln'];
				$email_valid = $_GET['e'];
				$user_valid = $_GET['u'];

			?>

				<!-- Form AFTER edit and for displaying validation errors -->
				<form method='POST' action='verify_edit.php' enctype='multipart/form-data'>

					<label class='edit_labels'>FIRSTNAME:</label>
					<input class='edit_fields' style='left: 128px;' type='text' name='fname' id='fname' value='<?php echo $firstname ?>'>

					<?php	# Firstname Validation

					if ($fname_valid == 0) {
						echo "<label style='color:red; position:relative; left:140px;' id='error_fname'>* Input Firstname</label><br/>";
					} else if ($fname_valid == 2) {
						echo "<label style='color:red; position:relative; left:140px;' id='error_fname'>* Name must not contain numbers and symbols</label><br/>";
					} else { echo "<br/>"; }

					?>

					<label class='edit_labels'>LASTNAME:</label>
					<input class='edit_fields' style='left: 133px;' type='text' name='lname' id='lname' value='<?php echo $lastname ?>'>

					<?php	# Lastname Validation

					if ($lname_valid == 0) {
						echo "<label style='color:red; position:relative; left:145px;' id='error_lname'>* Input Lastname</label><br/>";
					} else if ($lname_valid == 2) {
						echo "<label style='color:red; position:relative; left:145px;' id='error_lname'>* Name must not contain numbers and symbols</label><br/>";
					} else { echo "<br/>"; }

					?>

					<label class='edit_labels'>EMAIL ADDRESS:</label>
					<input class='edit_fields' style='left: 97px;' type='text' name='email' id='email' value='<?php echo $email ?>'>

					<?php 	# Email Validation

					if ($email_valid == 0) {
						echo "<label style='color:red; position:relative; left:108px;' id='error_email'>* Input Email Address</label><br/>";	
					} else if ($email_valid == 2) {
					    echo "<label style='color:red; position:relative; left:108px;' id='error_email'>* Email Address already in use</label><br/>";
					} else if ($email_valid == 3) {
						echo "<label style='color:red; position:relative; left:108px;' id='error_email'>* Email Address is invalid</label><br/>";
					} else { echo "<br/>"; }

					?>
					
					<label class='edit_labels'>USERNAME:</label>
					<input class='edit_fields' style='left: 130px;' type='text' name='username' id='username' value='<?php echo $username ?>'>

					<?php 	 #Username Validation

					if ($user_valid == 0) {
						echo "<label style='color:red; position:relative; left:141px;' id='error_user'>* Input Username</label><br/>";
					} else if ($user_valid == 2) {
						echo "<label style='color:red; position:relative; left:141px;' id='error_user'>* Username already in use</label><br/>";
					} else { echo "<br/>"; }

					?>

					<br/><br/>
					<label class='edit_labels' style='position:relative; top: -15px'>PROFILE PICTURE:</label>	
					<input style='position: relative; left: 88px; top: -15px' type='file' name='picture' id='picture'><br/><br/>

					<input class='edit_save' type='submit' name='calculate' id='calculate' value='SAVE CHANGES'>
				</form>
			
		<?php } else { ?>

				<!-- Form BEFORE editing -->
				<form method='POST' action='verify_edit.php' enctype='multipart/form-data'>
					<label class='edit_labels'>FIRSTNAME:</label>
					<input class='edit_fields' style='left: 128px;' type='text' name='fname' id='fname' value='<?php echo $firstname ?>'>
					<label style='color:red; position:relative; left:140px;' id='error_fname'></label><br/>
					<label class='edit_labels'>LASTNAME:</label>
					<input class='edit_fields' style='left: 133px;' type='text' name='lname' id='lname' value='<?php echo $lastname ?>'>
					<label style='color:red; position:relative; left:145px;' id='error_lname'></label><br/>
					<label class='edit_labels'>EMAIL ADDRESS:</label>
					<input class='edit_fields' style='left: 97px;' type='text' name='email' id='email' value='<?php echo $email ?>'>
					<label style='color:red; position:relative; left:108px;' id='error_email'></label><br/>
					<label class='edit_labels'>USERNAME:</label>
					<input class='edit_fields' style='left: 130px;' type='text' name='username' id='username' value='<?php echo $username ?>'>
					<label style='color:red; position:relative; left:141px;' id='error_user'></label><br/>

					<br/><br/>
					<label class='edit_labels' style='position:relative; top: -15px'>PROFILE PICTURE:</label>	
					<input style='position: relative; left: 88px; top: -15px' type='file' name='picture' id='picture'><br/><br/>
	
					<input class='edit_save' type='submit' name='calculate' id='calculate' value='SAVE CHANGES'>
				</form>
		
		<?php } ?>
			
		<a href="home.php"><button class="edit_cancel">CANCEL</button></a><br/>
		<a href="change_password.php"><button style="position:relative; left:20px">Change Password</button></a><br/>
		


	</div>

</body>
</html>

