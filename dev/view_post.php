<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

	?>

	<div class="main">
		
		<?php

			$pid = $_GET['p'];

			echo "<br/><button class='like-btn-click' style='position:relative; left:21px; top:5px;' onclick='window.history.go(-1); return false;'>BACK</button><br/><br/><br/>";

			$sql2 = "SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted FROM posts INNER JOIN users ON users.id=posts.user_id WHERE posts.id='$pid'";
			$result2 = $con->query($sql2);

	        while ($row2 = mysqli_fetch_assoc($result2)) {

		    	//counting likes
		    	$sql13 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$pid'";
				$result13 = $con->query($sql13);
		    	$row13 = mysqli_fetch_assoc($result13);

		    	//counting reposts
		    	$sql12 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$pid'";
				$result12 = $con->query($sql12);
		    	$row12 = mysqli_fetch_assoc($result12);

		    	//if liked or not
				$sql15 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result15 = $con->query($sql15);

				$you_liked = 0;

				while ($row15 = mysqli_fetch_assoc($result15)) {
					if ($row15['luid']==$id_logged && $row15['lpid']==$row2['pid'] && $row15['puid']==$row2['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql16 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result16 = $con->query($sql16);

				$you_reposted = 0;

				while ($row16 = mysqli_fetch_assoc($result16)) {
					if ($row16['ruid']==$id_logged && $row16['rpid']==$row2['pid'] && $row16['puid']==$row2['uid']) {
						$you_reposted = 1;
					}
				}

	        	$posted_date = explode ("-", $row2['date_posted']);
        		$posted_time = explode (":", $row2['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

			    ?>
				   	<div class='display-view-post-div'>
				   		<div>
					   		<img src='../img/users/<?php echo htmlspecialchars($row2['image']) ?>' class='display-post-div-image' />
					   	</div>
				       	<label class='display-post-div-label'><?php echo htmlspecialchars($row2['username']) ?></label>
				       	<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
						<div>
							<text class='display-post-div-text'><?php echo htmlspecialchars($row2['content']) ?></text><br/><br/>
						</div>
						<div class='like_repost_edit_delete'>

					<?php	if ($you_liked==1) : ?>
								<form method="POST" action="unlike_post.php?i=4">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row2['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row2['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
										</a>
									</label> &nbsp;&nbsp;
					<?php	else : ?>
								<form method="POST" action="like_post.php?i=4">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row2['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row2['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
										</a>
									</label> &nbsp;&nbsp;
					<?php 	endif;

						if ($you_reposted==1) : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row2['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row2['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php	else : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row2['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row2['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' value="Repost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php endif; ?>

							<a href='edit_post.php?u=<?php echo $row2['uid']."&p=".$row2['pid']."&i=4" ?>'><button class='like-btn-click' style='position:relative; left:760px; top:-107px'>Edit</button></a>
							<button id='deleteBtn' class='like-btn-click' style='position:relative; left:770px; top:-107px'>Delete</button>
						</div>
					</div>

				<?php


				$_SESSION['u'] = $row2['uid'];
				$_SESSION['p'] = $row2['pid'];
				$_SESSION['i'] = 4;  // id=4 for viewing post


				/*//The URL that we want to send a PUT request to.
				$url = 'http://localhost/tester/log.php';

				//Initiate cURL
				$ch = curl_init($url);

				//Use the CURLOPT_PUT option to tell cURL that
				//this is a PUT request.
				curl_setopt($ch, CURLOPT_PUT, true);

				//We want the result / output returned.
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//Our fields.
				$fields = array("id" => 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

				//Execute the request.
				$response = curl_exec($ch);

				echo $response;


				////////////////////
				$post = [
				    'username' => 'user1',
				    'password' => 'passuser1',
				    'gender'   => 1,
				];

				$ch = curl_init('http://www.example.com');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

				// execute!
				$response = curl_exec($ch);

				// close the connection, release resources used
				curl_close($ch);

				// do anything you want with your response
				var_dump($response);*/

	        }

		?>

			<!-- Delete Confirmation Modal -->
			<div id="myModal" class="modal">
				<!-- Modal content -->
				<div class="modal-content">
					<br/><p>Delete post?</p>
					<a href="delete_post.php"><button class="modal-btn-delete">Delete</button></a>
					<button id="cancelBtn" class="modal-btn-cancel">Cancel</button>
				</div>
			</div>

			<script>
				var modal = document.getElementById("myModal");
				var deletebtn = document.getElementById("deleteBtn");
				var cancelbtn = document.getElementById("cancelBtn");

				deletebtn.onclick = function() {
				    modal.style.display = "block";
				}

				cancelbtn.onclick = function() {
				    modal.style.display = "none";
				}

				window.onclick = function(event) {
				    if (event.target == modal) {
				        modal.style.display = "none";
				    }
				}
			</script>

		<br/>
	</div>

</body>
</html>