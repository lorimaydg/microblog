<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
	<script src="../lib/jquery/jquery-3.2.1.min.js"></script>
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

        if ($activation==0) {

        	session_destroy();

	        echo "
				<script type='text/javascript'>
					alert('Your account is not yet activated!');
					location = 'go_to_email.php';
				</script>
			";
	    }

	?>
	
	<div class="main">
		<br/>
		<div>
			<form method="POST" action="">
				<label class="write_post">Write Post</label><br/><br/>
				<textarea class="post_box" name="post_box" id="post_box" maxlength="140"></textarea>
				<input class="post-btn" type="submit" name="submit" id="submit" value="POST" />
			</form>
		</div>

		<?php 

			if (isset($_POST['submit'])) {

				$post_content = $_POST['post_box'];

				if ($post_content==null || strlen($post_content)>140) {
					echo "<script> alert('Post must not be empty or more than 140 characters'); </script>";
				} else {
					$sql2 = $con->prepare("INSERT INTO posts (user_id, content, date_posted) VALUES (?,?,?)");
					$sql2->bind_param("iss", $id_logged, $post_content, $current_date_time);		
					$sql2->execute();		

					header("Location: home.php");
					$sql2->close();	
				}
			}

			// PAGES //

			if (!isset($_GET['page'])) {
				$page = 1;
			} else {
				$page = $_GET['page'];
			}

			$count_own_posts_sql = $con->prepare("SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.content, posts.date_posted FROM users INNER JOIN posts ON posts.user_id = users.id WHERE user_id=?");
			$count_own_posts_sql->bind_param("i", $id_logged);
			$count_own_posts_sql->execute();
			$count_own_posts_result = $count_own_posts_sql->get_result();
			$count_own_posts = mysqli_num_rows($count_own_posts_result);

			$count_following_posts_sql = $con->prepare("SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.user_id AS puid, posts.content, posts.date_posted, followers.user_id AS fuid, followers.follower_id FROM users INNER JOIN posts ON users.id = posts.user_id INNER JOIN followers ON posts.user_id=follower_id WHERE followers.user_id=?");
			$count_following_posts_sql->bind_param("i", $id_logged);
			$count_following_posts_sql->execute();
			$count_following_posts_result = $count_following_posts_sql->get_result();
			$count_following_posts = mysqli_num_rows($count_following_posts_result);

			$count_reposts_sql = $con->prepare("SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.user_id AS puid, posts.content, posts.date_posted, reposts.user_id, reposts.post_id, followers.user_id AS fuid, followers.follower_id FROM users INNER JOIN posts ON users.id = posts.user_id INNER JOIN reposts ON reposts.post_id=posts.id INNER JOIN followers ON posts.user_id=follower_id WHERE followers.user_id='$id_logged' AND reposts.user_id=?");
			$count_reposts_sql->bind_param("i", $id_logged);
			$count_reposts_sql->execute();
			$count_reposts_result = $count_reposts_sql->get_result();
			$count_reposts = mysqli_num_rows($count_reposts_result);

			$count_following_reposts_sql = $con->prepare("SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted, users.id AS uid, users.username AS username, users.image, followers.follower_id AS ffid, followers.user_id AS fuid FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id INNER JOIN followers ON followers.user_id=reposts.user_id WHERE followers.follower_id=?");
			$count_following_reposts_sql->bind_param("i", $id_logged);
			$count_following_reposts_sql->execute();
			$count_following_reposts_result = $count_following_reposts_sql->get_result();
			$count_following_reposts = mysqli_num_rows($count_following_reposts_result);

			//total_items_per_page = 8;
			$total_posts_to_display = $count_own_posts + $count_following_posts + $count_reposts + $count_following_reposts;
			$lastpage = (int)($total_posts_to_display/8);
			$mod = (int)($total_posts_to_display%8);

			//echo $total_posts_to_display." ".$lastpage." ".$mod;

			if ($mod!=0) {
				$lastpage = $lastpage+1;
			}

			$next_page = $page+1;
			$prev_page = $page-1;

			$items_per_page = 2;
			$offset = ($page-1) * $items_per_page;

			$disable_next_btn = 0;
			$disable_prev_btn = 0;

			if ($prev_page<1) {
				if ($page==1) {
					$disable_prev_btn = 1;
				} else {
					$disable_prev_btn = 0;
				}
			}

			if ($page<$lastpage) {
				$disable_next_btn = 0;
			} else if ($page==$lastpage) {
				$disable_next_btn = 1;
			} 


			if ($disable_prev_btn==1) :
				if ($disable_next_btn==1) : ?>
						&nbsp;&nbsp; <a href='home.php?page=<?php echo $prev_page ?>'><button id='prev_btn' class='like-btn-click' style='position:relative;left:350px' disabled><<</button></a>
						<label style='font-family: Helvetica; font-size: 14px; position:relative;left:350px'>&nbsp;&nbsp; Page <?php echo $page." of ".$lastpage ?> &nbsp;&nbsp; </label>
						<a href='home.php?page=<?php echo $next_page ?>'><button id='next_btn' class='like-btn-click' style='position:relative;left:350px' disabled>>></button></a> &nbsp;
				<?php else : ?>
						&nbsp;&nbsp; <a href='home.php?page=<?php echo $prev_page ?>'><button id='prev_btn' class='like-btn-click' style='position:relative;left:350px' disabled><<</button></a>
						<label style='font-family: Helvetica; font-size: 14px; position:relative;left:350px'>&nbsp;&nbsp; Page <?php echo $page." of ".$lastpage ?> &nbsp;&nbsp; </label>
						<a href='home.php?page=<?php echo $next_page ?>'><button id='next_btn' class='like-btn-click' style='position:relative;left:350px'>>></button></a> &nbsp;
				<?php endif;
			elseif ($disable_next_btn==1) : ?>
					&nbsp;&nbsp; <a href='home.php?page=<?php echo $prev_page ?>'><button id='prev_btn' class='like-btn-click' style='position:relative;left:350px'><<</button></a>
					<label style='font-family: Helvetica; font-size: 14px; position:relative;left:350px'>&nbsp;&nbsp; Page <?php echo $page." of ".$lastpage ?> &nbsp;&nbsp; </label>
					<a href='home.php?page=<?php echo $next_page ?>'><button id='next_btn' class='like-btn-click' style='position:relative;left:350px' disabled>>></button></a> &nbsp;
			<?php else : ?>
					&nbsp;&nbsp; <a href='home.php?page=<?php echo $prev_page ?>'><button id='prev_btn' class='like-btn-click' style='position:relative;left:350px'><<</button></a>
					<label style='font-family: Helvetica; font-size: 14px; position:relative;left:350px'>&nbsp;&nbsp; Page <?php echo $page." of ".$lastpage ?> &nbsp;&nbsp; </label>
					<a href='home.php?page=<?php echo $next_page ?>'><button id='next_btn' class='like-btn-click' style='position:relative;left:350px'>>></button></a> &nbsp;
			<?php endif; 

			// END OF PAGES //

		?>

		<br/><br/>

		<?php

			$no_posts = 0;

			//START OF DISPLAYING OWN POSTS//

			$sql25 = "SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted FROM users INNER JOIN posts ON posts.user_id = users.id WHERE user_id='$id_logged' ORDER BY posts.date_posted DESC LIMIT $offset,$items_per_page";
			$result25 = $con->query($sql25);

		    while ($row25 = mysqli_fetch_assoc($result25)) {

		    	$pid = $row25['pid'];

		    	//counting likes
		    	$sql26 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$pid'";
				$result26 = $con->query($sql26);
		    	$row26 = mysqli_fetch_assoc($result26);

		    	//counting reposts
		    	$sql27 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$pid'";
				$result27 = $con->query($sql27);
		    	$row27 = mysqli_fetch_assoc($result27);

		    	//if liked or not
				$sql28 = $con->prepare("SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id");
		    	$sql28->execute();

				$you_liked = 0;

				$result28 = $sql28->get_result();
				while ($row28 = mysqli_fetch_assoc($result28)) {
					if ($row28['luid']==$id_logged && $row28['lpid']==$row25['pid'] && $row28['puid']==$row25['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql29 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result29 = $con->query($sql29);

				$you_reposted = 0;

				while ($row29 = mysqli_fetch_assoc($result29)) {
					if ($row29['ruid']==$id_logged && $row29['rpid']==$row25['pid'] && $row29['puid']==$row25['uid']) {
						$you_reposted = 1;
					}
				}

				$posted_date = explode ("-", $row25['date_posted']);
        		$posted_time = explode (":", $row25['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

		        ?>
				 
				<div class='display-view-post-div'>
				  	<div>
						<img src='../img/users/<?php echo htmlspecialchars($row25['image']) ?>' class='display-post-div-image' />
					</div>
				    <label class='display-post-div-label'><?php echo htmlspecialchars($row25['username']) ?></label>
				   	<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
					<div>
						<text class='display-post-div-text'><?php echo htmlspecialchars($row25['content']) ?></text><br/><br/>
					</div>
					<div class='like_repost_edit_delete'>

				<?php

				if ($you_liked==1) : ?>
						<form method="POST" action="unlike_post.php?page=<?php echo $page ?>&i=1">
							<label id='like_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row26['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid" value="<?php echo $row25['uid'] ?>">
									<input type="hidden" name="pid" value="<?php echo $row25['pid'] ?>">
									<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
								</a>
							</label> &nbsp;&nbsp;
				<?php else : ?>
						<form method="POST" action="like_post.php?page=<?php echo $page ?>&i=1">
							<label id='like_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row26['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid" value="<?php echo $row25['uid'] ?>">
									<input type="hidden" name="pid" value="<?php echo $row25['pid'] ?>">
									<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
								</a>
							</label> &nbsp;&nbsp;
				<?php endif; 

				if ($you_reposted==1) : ?>
							<label id='repost_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row27['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid2" value="<?php echo $row25['uid'] ?>">
									<input type="hidden" name="pid2" value="<?php echo $row25['pid'] ?>">
									<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
								</a>
							</label> &nbsp;&nbsp;
						</form>
					</div>
				</div>
				<?php else : ?>
							<label id='repost_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row27['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid2" value="<?php echo $row25['uid'] ?>">
									<input type="hidden" name="pid2" value="<?php echo $row25['pid'] ?>">
									<input type="submit" name="action2" id="action2" class='like-btn-click' value="Repost" />
								</a>
							</label> &nbsp;&nbsp;
						</form>
					</div>
				</div>
				<?php endif; 
		    }

			//END OF DISPLAYING OWN POSTS//

		    //START OF DISPLAYING FOLLOWING POSTS //
			$sql6 = "SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.user_id AS puid, posts.content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted, followers.user_id AS fuid, followers.follower_id FROM users INNER JOIN posts ON users.id = posts.user_id INNER JOIN followers ON posts.user_id=follower_id WHERE followers.user_id='$id_logged' ORDER BY posts.date_posted DESC LIMIT $offset,$items_per_page";
			$result6 = $con->query($sql6);

		    while ($row6 = mysqli_fetch_assoc($result6)) {
		    	
		    	// FOR POSTING //
		    	$uid = $row6['uid'];
		    	$pid = $row6['pid'];

		    	//counting likes
		    	$sql13 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$pid'";
				$result13 = $con->query($sql13);
		    	$row13 = mysqli_fetch_assoc($result13);

		    	//counting reposts
		    	$sql12 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$pid'";
				$result12 = $con->query($sql12);
		    	$row12 = mysqli_fetch_assoc($result12);

		    	//if liked or not
				$sql15 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result15 = $con->query($sql15);

				$you_liked = 0;

				while ($row15 = mysqli_fetch_assoc($result15)) {
					if ($row15['luid']==$id_logged && $row15['lpid']==$row6['pid'] && $row15['puid']==$row6['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql16 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result16 = $con->query($sql16);

				$you_reposted = 0;

				while ($row16 = mysqli_fetch_assoc($result16)) {
					if ($row16['ruid']==$id_logged && $row16['rpid']==$row6['pid'] && $row16['puid']==$row6['uid']) {
						$you_reposted = 1;
					}
				}

		  	    $posted_date = explode ("-", $row6['date_posted']);
        		$posted_time = explode (":", $row6['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

				$acc_to_view = $row6['uid'];
		        
		        //posting
		        if ($you_reposted!=1) : ?>

				       	<div class='display-view-post-div'>
				       		<div>
					   			<img src='../img/users/<?php echo htmlspecialchars($row6['image']) ?>' class='display-post-div-image' />
					   		</div>
				        	<label class='display-post-div-label'><a href='view_user_profiles.php?id=$acc_to_view' style='text-decoration:none; color:black'><?php echo htmlspecialchars($row6['username']) ?></a></label>
				        	<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
							<div>
								<text class='display-post-div-text'><?php echo htmlspecialchars($row6['content']) ?></text><br/><br/>
							</div>
							<div class='like_repost_edit_delete'>

							<?php if ($you_liked==1) : ?>
								<form method="POST" action="unlike_post.php?page=<?php echo $page ?>&i=1">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
										</a>
									</label> &nbsp;&nbsp;
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
							<?php else : ?>
								<form method="POST" action="like_post.php?page=<?php echo $page ?>&i=1">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
										</a>
									</label> &nbsp;&nbsp;
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
							<?php endif; ?>
						</div>
					</div>

				<?php endif; 

				//$no_posts = 1;

		    }

			// END OF DISPLAYING FOLLOWING POSTS //

			// START OF DISPLAYING REPOSTS //

		    $sql17 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted, users.id AS uid, users.username AS username, users.image, followers.follower_id AS ffid, followers.user_id AS fuid FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id INNER JOIN followers ON followers.user_id=reposts.user_id WHERE followers.follower_id='$id_logged' ORDER BY RAND() LIMIT $offset,$items_per_page";
			$result17 = $con->query($sql17);

			while ($row17 = mysqli_fetch_assoc($result17)) {

				$r_uid = $row17['uid'];
		    	$r_pid = $row17['pid'];

		    	//counting likes
		    	$sql20 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$r_pid'";
				$result20 = $con->query($sql20);
		    	$row20 = mysqli_fetch_assoc($result20);

		    	//counting reposts
		    	$sql21 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$r_pid'";
				$result21 = $con->query($sql21);
		    	$row21 = mysqli_fetch_assoc($result21);

		    	//if liked or not
				$sql22 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result22 = $con->query($sql22);

				$you_liked = 0;

				while ($row22 = mysqli_fetch_assoc($result22)) {
					if ($row22['luid']==$id_logged && $row22['lpid']==$row17['pid'] && $row22['puid']==$row17['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql23 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id WHERE reposts.user_id='$id_logged'";
				$result23 = $con->query($sql23);

				$you_reposted = 0;

				while ($row23 = mysqli_fetch_assoc($result23)) {
					if ($row23['ruid']==$id_logged) {
						$you_reposted = 1;
					} 
 				}

		       	$posted_date = explode ("-", $row17['date_posted']);
        		$posted_time = explode (":", $row17['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

		        $acc_to_view = $row17['uid'];

		        //reposting
		        if ($you_reposted==1) { ?>
				       	<div class='display-view-post-div'>
					       	<div>
					   			<img src='../img/users/<?php echo htmlspecialchars($row17['image']) ?>' class='display-post-div-image' />
					   		</div>

					<?php

						if ($row17['username']==$username) {
					        echo "<label class='display-post-div-label'>You reposted your own post</label>";
						} else {
							echo "<label class='display-post-div-label'>You reposted <a href='view_user_profiles.php?id=".$acc_to_view."' style='color:black'>".htmlspecialchars($row17['username'])."</a>'s post</label>";
						}

					?>
				        	<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
							<div>
								<text class='display-post-div-text'><?php echo htmlspecialchars($row17['content']) ?></text><br/><br/>
							</div>
							<div class='like_repost_edit_delete'>

					<?php if ($you_liked==1) : ?>
								<form method="POST" action="unlike_post.php?page=<?php echo $page ?>&i=1">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row20['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row17['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row17['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
										</a>
									</label> &nbsp;&nbsp;
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row21['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row17['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row17['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
							</div>
						</div>
					<?php else : ?>
								<form method="POST" action="like_post.php?page=<?php echo $page ?>&i=1">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row20['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row17['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row17['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
										</a>
									</label> &nbsp;&nbsp;
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row21['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row17['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row17['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
							</div>
						</div>
					<?php endif;
				}
			}

			// END OF DISPLAYING REPOSTS //

			// START OF DISPLAYING FOLLOWING REPOSTS //

			$sql30 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted, users.id AS uid, users.username AS username, users.image, followers.follower_id AS ffid, followers.user_id AS fuid FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id INNER JOIN followers ON followers.user_id=reposts.user_id WHERE followers.follower_id='$id_logged' ORDER BY RAND() LIMIT $offset,$items_per_page";
			$result30 = $con->query($sql30);

			while ($row30 = mysqli_fetch_assoc($result30)) {

				$r_uid = $row30['uid'];
		    	$r_pid = $row30['pid'];

		    	//counting likes
		    	$sql31 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$r_pid'";
				$result31 = $con->query($sql31);
		    	$row31 = mysqli_fetch_assoc($result31);

		    	//counting reposts
		    	$sql32 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$r_pid'";
				$result32 = $con->query($sql32);
		    	$row32 = mysqli_fetch_assoc($result32);

		    	//if liked or not
				$sql33 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result33 = $con->query($sql33);

				$you_liked = 0;

				while ($row33 = mysqli_fetch_assoc($result33)) {
					if ($row33['luid']==$id_logged && $row33['lpid']==$row30['pid'] && $row33['puid']==$row30['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql34 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result34 = $con->query($sql34);

				$you_reposted = 0;

				while ($row34 = mysqli_fetch_assoc($result34)) {
					if ($row34['ruid']==$id_logged && $row34['rpid']==$row30['pid'] && $row34['puid']==$row30['uid']) {
						$you_reposted = 1;
					}
				}

				$posted_date = explode ("-", $row30['date_posted']);
        		$posted_time = explode (":", $row30['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

		        $user_who_reposted = $row30['ruid'];
		        $sql35 = "SELECT * FROM users WHERE id='$user_who_reposted'";
		        $result35 = $con->query($sql35);

				while ($row35 = mysqli_fetch_assoc($result35)) {
					$user_who_reposted_user = $row35['username'];
					$user_who_reposted_img = $row35['image'];
					$user_who_reposted_id = $row35['id'];
 				}

		        //reposting
		        echo "
				    <div class='display-view-post-div'>
				    	<div>
							<img src='../img/users/".htmlspecialchars($user_who_reposted_img)."' class='display-post-div-image' />
						</div>
				";


				if ($gender==1) {
		        	$his_her = 'his';
		        }
		        if ($gender==2) {
		        	$his_her = 'her';
		        }
				
				if ($row30['username']==$user_who_reposted_user) {
					echo "<label class='display-post-div-label'><a href='view_user_profiles.php?id=$user_who_reposted_id' style='color:black'>".htmlspecialchars($user_who_reposted_user)."</a> reposted ".$his_her." own post</label>";
				} else if ($row30['username']==$username) {
					echo "<label class='display-post-div-label'><a href='view_user_profiles.php?id=$user_who_reposted_id' style='color:black'>".htmlspecialchars($user_who_reposted_user)."</a> reposted your post</label>";
				} else {
					echo "<label class='display-post-div-label'><a href='view_user_profiles.php?id=$user_who_reposted_id' style='color:black'>".htmlspecialchars($user_who_reposted_user)."</a> reposted <a href='view_user_profiles.php?id=".$row30['uid']."' style='color:black'>".$row30['username']."</a>'s post</label>";
				}
				
				?>      	
				
					<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
						<div>
							<text class='display-post-div-text'><?php echo htmlspecialchars($row30['content']) ?></text><br/><br/>
						</div>
						<div class='like_repost_edit_delete'>

				<?php
				if ($you_liked==1) : ?>
						<form method="POST" action="unlike_post.php?page=<?php echo $page ?>&i=1">
							<label id='like_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row31['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid" value="<?php echo $row30['uid'] ?>">
									<input type="hidden" name="pid" value="<?php echo $row30['pid'] ?>">
									<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
								</a>
							</label> &nbsp;&nbsp;
				<?php else : ?>
						<form method="POST" action="like_post.php?page=<?php echo $page ?>&i=1">
							<label id='like_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row31['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid" value="<?php echo $row30['uid'] ?>">
									<input type="hidden" name="pid" value="<?php echo $row30['pid'] ?>">
									<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
								</a>
							</label> &nbsp;&nbsp;
				<?php endif; 

				if ($you_reposted==1) : ?>
							<label id='repost_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row32['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid2" value="<?php echo $row30['uid'] ?>">
									<input type="hidden" name="pid2" value="<?php echo $row30['pid'] ?>">
									<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
								</a>
							</label> &nbsp;&nbsp;
						</form>
					</div>
				</div>
				<?php else : ?>
							<label id='repost_text'>
								<a href='' class='display-post-div-btn'>
									<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row32['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
									<input type="hidden" name="uid2" value="<?php echo $row30['uid'] ?>">
									<input type="hidden" name="pid2" value="<?php echo $row30['pid'] ?>">
									<input type="submit" name="action2" id="action2" class='like-btn-click' value="Repost" />
								</a>
							</label> &nbsp;&nbsp;
						</form>
					</div>
				</div>
				<?php endif; 	
			}

			// END OF DISPLAYING FOLLOWING REPOSTS //


 		    /* if ($no_posts == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Posts</label>";

		    }*/

			$con->close(); 

		?>
			
		<br/>
	</div>

</body>
</html>	