<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	include ('db.php');

	$id_logged = $_SESSION['id_logged'];
	$user_to_unfollow = $_SESSION['follow'];

	$sql = $con->prepare("DELETE FROM followers WHERE follower_id=? AND user_id=?");
	$sql->bind_param("ii", $user_to_unfollow, $id_logged);		
	$sql->execute();

   	header("Location: view_user_profiles.php?id=".$user_to_unfollow);

	$con->close();

?>