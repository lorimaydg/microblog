<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar-notif.php');

	?>

	<div class="main">

		<div>
			<!-- <a href="notification.php" class="text_header_notif">All Notifications</a>&nbsp;&nbsp;&nbsp; -->
			<a href="" class="text_header_notif_active">Follows</a>&nbsp;&nbsp;&nbsp;
			<a href="like_tab_notif.php" class="text_header_notif">Likes</a>&nbsp;&nbsp;&nbsp;
			<a href="repost_tab_notif.php" class="text_header_notif">Reposts</a><br/><br/><br/><br/>
		</div>

		<?php 

			$no_notifs = 0;

			$sql6 = "SELECT users.id, users.username, users.image, followers.user_id, followers.follower_id, date(followers.date_followed) AS date_followed, time(followers.date_followed) AS time_followed FROM followers INNER JOIN users ON users.id=followers.user_id WHERE followers.follower_id='$id_logged' ORDER BY followers.date_followed DESC";
			$result6 = $con->query($sql6);

	        while ($row6 = mysqli_fetch_assoc($result6)) {
	        	$posted_date = explode ("-", $row6['date_followed']);
        		$posted_time = explode (":", $row6['time_followed']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

		      	echo "
			       	<div class='display-view-post-div'>
			       		<text class='display-notif-div-text'><strong>".htmlspecialchars($row6['username'])."</strong> followed you</text>
			        	<label class='display-notif-div-date'>".htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm)."</label><br/>
			        	<a href='view_user_profiles.php?id=".$row6['id']."'><button class='display-follow-notif-div-btn'>View Profile</button></a><br/>
					</div>
				";
				$no_notifs = 1;
	        }

	        if ($no_notifs == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Notifs</label>";
		    }

			$con->close(); 

		?>
			
		<br/>
	</div>

</body>
</html>