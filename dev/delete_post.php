
<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	include ('db.php');

	$uid = $_SESSION['u'];
	$pid = $_SESSION['p'];
	$i = $_SESSION['i'];

	$sql = $con->prepare("DELETE FROM posts WHERE user_id=? AND id=?");
	$sql->bind_param("ii", $uid, $pid);		
	$sql->execute();
	$sql->close();

	if ($i==1) {
		header("Location: all_post.php");
	} else if ($i==4) {
		echo "<script> window.history.go(-2); </script>";
	}

	
	$con->close();
	
?>