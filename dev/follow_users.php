<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	include ('db.php');

	$id_logged = $_SESSION['id_logged'];
	$user_to_follow = $_SESSION['follow'];

	date_default_timezone_set('Asia/Manila');
	$date_followed = date ('Y-m-d h:i:s');

	$sql = $con->prepare("INSERT INTO followers (user_id, follower_id, date_followed) VALUES (?,?,?)");
	$sql->bind_param("iis", $id_logged, $user_to_follow, $date_followed);
	$sql->execute();

    header("Location: view_user_profiles.php?id=".$user_to_follow);

	$sql->close();
	$con->close();

?>