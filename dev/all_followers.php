<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>
	
	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

	?>

	<div class="main">
		
		<div>
			<label class="text_header">Who follows you:</label><br/><br/><br/><br/>
		</div>

		<?php 

			$no_followers = 0;

			$sql2 = "SELECT users.id, users.firstname, users.lastname, users.username, users.image, followers.user_id, followers.follower_id FROM followers INNER JOIN users ON users.id=followers.user_id WHERE followers.follower_id='$id_logged'";	
			$result2 = $con->query($sql2);

		    while ($row2 = mysqli_fetch_assoc($result2)) {
		    	echo "
		    		<div class='display-search-div'>
				    	<img src='../img/users/".$row2['image']."' class='display-search-div-img' />
				    	<label class='display-search-div-label'>".$row2['firstname']." ".$row2['lastname']."</label><br/>
				    	<label class='display-search-div-text'>".$row2['username']."</label><br/>
				    	<a href='view_user_profiles.php?id=".$row2['id']."'><button class='display-search-div-btn'>View Profile</button></a>
					</div>
				";

				$no_followers = 1;
		    }

		    if ($no_followers == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Followers</label>";
		    }

			$con->close(); 

		?>
			
		<br/>
	</div>

</body>
</html>