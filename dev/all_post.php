<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>

	<title>MicroBlog</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
	<script src="../lib/jquery/jquery-3.2.1.min.js"></script>

	<script>

		function deleteBtnClicked() {

			var modal = document.getElementById("myModal");
			var cancelbtn = document.getElementById("cancelBtn");
		
			modal.style.display = "block";

			cancelbtn.onclick = function() {
			    modal.style.display = "none";
			}

			window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.style.display = "none";
			    }
			}
		}

		function deletePost() {
			window.location = "delete_post.php";
		}
	
	</script>

	<!-- Delete Confirmation Modal -->
	<div id="myModal" class="modal">
		<!-- Modal content -->
		<div class="modal-content">
			<br/><p>Delete post?</p>
				<button id="DeletePost" class="modal-btn-delete" onclick="deletePost()">Delete</button>
			<button id="cancelBtn" class="modal-btn-cancel">Cancel</button>
		</div>
	</div>

</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

	?>

	<div class="main">
		
		<div>
			<label class="text_header">All Posts</label><br/><br/><br/><br/>
		</div>

		<?php 

			$no_posts = 0;

			$sql6 = "SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted FROM users INNER JOIN posts ON posts.user_id = users.id WHERE user_id='$id_logged' ORDER BY posts.date_posted DESC";
			$result6 = $con->query($sql6);

		    while ($row6 = mysqli_fetch_assoc($result6)) {

		    	$pid = $row6['pid'];

		    	//counting likes
		    	$sql13 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$pid'";
				$result13 = $con->query($sql13);
		    	$row13 = mysqli_fetch_assoc($result13);

		    	//counting reposts
		    	$sql12 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$pid'";
				$result12 = $con->query($sql12);
		    	$row12 = mysqli_fetch_assoc($result12);

		    	//if liked or not
				$sql15 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result15 = $con->query($sql15);

				$you_liked = 0;

				while ($row15 = mysqli_fetch_assoc($result15)) {
					if ($row15['luid']==$id_logged && $row15['lpid']==$row6['pid'] && $row15['puid']==$row6['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql16 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result16 = $con->query($sql16);

				$you_reposted = 0;

				while ($row16 = mysqli_fetch_assoc($result16)) {
					if ($row16['ruid']==$id_logged && $row16['rpid']==$row6['pid'] && $row16['puid']==$row6['uid']) {
						$you_reposted = 1;
					}
				}

		       	$posted_date = explode ("-", $row6['date_posted']);
        		$posted_time = explode (":", $row6['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

			    ?>
				  	<div class='display-view-post-div'>
				   		<div>
				   			<img src='../img/users/<?php echo htmlspecialchars($row6['image']) ?>' class='display-post-div-image' />
				   		</div>
					    <label class='display-post-div-label'><?php echo htmlspecialchars($row6['username']) ?></label>
					    <label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[0])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
						<div>
							<text class='display-post-div-text'><?php echo htmlspecialchars($row6['content']) ?></text><br/><br/>
						</div>
						<div class='like_repost_edit_delete'>

					<?php	if ($you_liked==1) : ?>
								<form method="POST" action="unlike_post.php?i=3">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
										</a>
									</label> &nbsp;&nbsp;
					<?php	else : ?>
								<form method="POST" action="like_post.php?i=3">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
										</a>
									</label> &nbsp;&nbsp;
					<?php 	endif;

						if ($you_reposted==1) : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php	else : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row12['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' value="Repost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php endif; 

					$_SESSION['u'] = $row6['uid'];
					$_SESSION['p'] = $row6['pid'];
					$_SESSION['i'] = 1; //id=1 for redirecting back to all_post.php

					echo "	
							<a href='edit_post.php?u=".$row6['uid']."&p=".$row6['pid']."&i=1'><button class='like-btn-click' style='position:relative; left:760px; top:-107px'>Edit</button></a>
							<button class='like-btn-click' style='position:relative; left:770px; top:-107px' onclick=\"deleteBtnClicked()\">Delete</button>
						</div>
					</div>		
				";

				$no_posts = 1;

		    }

		    if ($no_posts == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Posts</label>";
		    }

			$con->close(); 

		?>
			
			

		<br/>
	</div>

</body>
</html>