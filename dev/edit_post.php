<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar-disabled.php');

	?>
	<div class="main">

		<br/><br/>

		<?php

			$uid = $_GET['u'];
			$pid = $_GET['p'];
			$i = $_GET['i'];

			$sql2 = "SELECT * FROM posts WHERE user_id='$uid'";
			$result2 = $con->query($sql2);

			while ($row2 = mysqli_fetch_assoc($result2)) {
				if ($row2['id']==$pid) {
					$content = $row2['content'];
				}
			}

		?>

		<form method="POST" action="">
			<label class="write_post">Edit Post</label><br/><br/>
			<textarea class='post_box' name='post_box' id='post_box' maxlength='140'><?php echo $content ?></textarea>
			<input class="edit_save_post" type="submit" name="calculate" id="calculate" value="SAVE CHANGES">
		</form>
		
		<?php

			if ($i==1) {
				echo "<a href='all_post.php'><button class='edit_cancel_post'>CANCEL</button></a>";
			} else if ($i==4) {
				echo "<a href='view_post.php?p=$pid'><button class='edit_cancel_post'>CANCEL</button></a>";
			}

			if (isset($_POST['calculate'])) {
            
			    $content = $_POST['post_box'];

		        $sql = $con->prepare("UPDATE posts SET content=? WHERE id=? AND user_id=?");
				$sql->bind_param("sii", $content, $pid, $uid);		
				$sql->execute();

			    if ($i==1) : ?>
					<script type='text/javascript'>
						alert('Successfully edited post!');
						location.href = 'all_post.php';
					</script>
			    <?php elseif ($i==2) : ?>
					<script type='text/javascript'>
						alert('Successfully edited post!');
						location.href = 'view_post.php?p=$pid';
					</script>
			    <?php endif;
		    }

			$con->close();

		?>

	</div>

</body>
</html>

