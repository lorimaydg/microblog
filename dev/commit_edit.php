<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	$id_logged = $_SESSION['id_logged'];

	include ('db.php');

	$image = $_GET['p'];
	$new_fname = $_GET['fn'];
	$new_lname = $_GET['ln'];
	$new_email = $_GET['e'];
	$new_username = $_GET['u'];
	$email_changed = $_GET['ec'];

		//if email address was changed then redirect to email activation 
		if ($email_changed==0) {
			// if image is null then do not change image
			$activation=1;
			if ($image==null) {
				$sql = $con->prepare("UPDATE users SET firstname=?, lastname=?, email=?, username=?, activation=? WHERE id=?");
				$sql->bind_param("ssssii", $new_fname, $new_lname, $new_email, $new_username, $activation, $id_logged);
				$sql->execute(); ?>
				
				<script type='text/javascript'>
					alert('Successfully updated Profile Information!');
					location.href = 'home.php';
				</script>

			<?php
			} else {
				$sql = $con->prepare("UPDATE users SET firstname=?, lastname=?, email=?, username=?, activation=?, image=? WHERE id=?");
				$sql->bind_param("ssssisi", $new_fname, $new_lname, $new_email, $new_username, $activation, $image, $id_logged);		
				$sql->execute(); ?>

				<script type='text/javascript'>
					alert('Successfully updated Profile Information!');
					location.href = 'home.php';
				</script>
			
			<?php
			}
		} else if ($email_changed==1) {
			$activation=0;
			if ($image==null) {
				$sql = $con->prepare("UPDATE users SET firstname=?, lastname=?, email=?, username=?, activation=? WHERE id=?");
				$sql->bind_param("ssssii", $new_fname, $new_lname, $new_email, $new_username, $activation, $id_logged);		
				$sql->execute();

				$_SESSION['to_be_validated'] = $new_email; ?>
				
				<script type='text/javascript'>
					alert('Successfully updated Profile Information!');
					location.href = '../lib/PHPMailer/email_activation.php';
				</script>
			
			<?php
			} else {
				$sql = $con->prepare("UPDATE users SET firstname=?, lastname=?, email=?, username=?, activation=?, image=? WHERE id=?");
				$sql->bind_param("ssssisi", $new_fname, $new_lname, $new_email, $new_username, $activation, $image, $id_logged);		
				$sql->execute();

				$_SESSION['to_be_validated'] = $new_email; ?>

				<script type='text/javascript'>
					alert('Successfully updated Profile Information!');
					location.href = '../lib/PHPMailer/email_activation.php';
				</script>
			<?php
			}
		}
	
	$con->close();

?>

</body>
</html>

