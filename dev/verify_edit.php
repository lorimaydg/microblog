<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	if (isset($_POST['calculate'])) {
		$new_fname = $_POST['fname'];
		$new_lname = $_POST['lname'];
		$new_email = $_POST['email'];
		$new_username = $_POST['username'];

		$image = $_FILES['picture']['name'];
		$target_dir = "/test/MicroBlog/img/users/";				
		$imagepath = $target_dir.$image;

        $folder_dir = "C:/xampp/htdocs".$target_dir;
        if (move_uploaded_file($_FILES['picture']['tmp_name'], $folder_dir.$image)) {
            copy($folder_dir.$image, $folder_dir.$image);
        }
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
	<script src="../lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
	<script src="../lib/JavaScript-MD5-master/js/md5.min.js"></script>
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar-disabled.php');

	?>

	<div class="main">

		<form method="POST" action="">
			<br/><br/><label class="edit_labels" style="font-size: 25px">TYPE PASSWORD FOR VERIFICATION:</label><br/><br/>
			<input class="edit_fields" style="left:20px" type="password" name="input_pass" id="input_pass">

			<input type="hidden" name="new_fname" value="<?php echo $new_fname ?>" />
			<input type="hidden" name="new_lname" value="<?php echo $new_lname ?>" />
			<input type="hidden" name="new_email" value="<?php echo $new_email ?>" />
			<input type="hidden" name="new_username" value="<?php echo $new_username ?>" />
			<input type="hidden" name="new_picture" value="<?php echo $image ?>" />
			

			<br/><br/>
			<input class="edit_save" style="left:19px" type="submit" name="verify" id="verify" value="VERIFY" />
		</form>

		<a href="edit_profile.php"><button class="edit_cancel" style="color: black; background-color: white; left: 225px; border: 1px solid black">CANCEL</button></a>

		<?php

			if (isset($_POST['verify'])) {

				$new_fname = $_POST['new_fname'];
				$new_lname = $_POST['new_lname'];
				$new_email = $_POST['new_email'];
				$new_username = $_POST['new_username'];
				$new_picture = $_POST['new_picture'];
				$input_pass = $_POST['input_pass'];

				$fname_valid = 0;
				$lname_valid = 0;
				$user_valid = 0;


				// NAME VALIDATION //

				  	if ($new_fname == null) {
				  		$fname_valid = 0;
				    } else if (!preg_match("/^[a-zA-Z ]*$/",$new_fname)) {
				    	$fname_valid = 2;
					} else {
				       	$fname_valid = 1;
				    }

				    if ($new_lname == null) {
				    	$lname_valid = 0;
				    } else if (!preg_match("/^[a-zA-Z ]*$/",$new_lname)) {
				    	$lname_valid = 2;
					} else {
				       	$lname_valid = 1;
				    }

				// END OF NAME VALIDATION //

				// EMAIL VALIDATION //

					$sql6 = "SELECT * FROM users";
					$result6 = $con->query($sql6);

					$email_valid = 0;
			        $email_in_use = 0;
			        $email_changed = 0;


			        if ($email==$new_email) {
				    	$email_valid = 1;
				    } else {
				        while ($row6 = mysqli_fetch_assoc($result6)) {
					        if ($row6['email'] == $new_email) {
					        	$email_in_use = 1;	
					        }
					    }
					    $email_changed = 1;
					}

				    if ($new_email == null) {
				    	$email_valid = 0;
			        } else {
			        	$new_email = filter_var($new_email, FILTER_SANITIZE_EMAIL);
				        if ($email_in_use==1) {
				        	$email_valid = 2;
				    	} else if (!filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
				    		$email_valid = 3;
				        } else {
				        	$email_valid = 1;
				        }
				    }

				// END OF EMAIL VALIDATION //

				// USERNAME VALIDATION //

				    $sql7 = "SELECT * FROM users";
				    $result7 = $con->query($sql7);

				    $user_in_use = 0;

				    if ($username==$new_username) {
				    	$user_valid = 1;
				    } else {
				        while ($row7 = mysqli_fetch_assoc($result7)) {
					        if ($row7['username'] == $new_username) {
					        	$user_in_use = 1;	
					        }
					    }
					}

				    if ($new_username == null) {
				    	$user_valid = 0;
				    } else {
				    	if ($user_in_use == 1) {
				    		$user_valid = 2;
					    } else {
					    	$user_valid = 1;
					    }
				    }
							    
				// END OF USERNAME VALIDATION //


				if ($fname_valid==1 && $lname_valid==1 && $email_valid==1 && $user_valid==1) {
					if (MD5($input_pass)==$password) {
						if ($new_picture==null) {
							echo "
								<script>
									window.location = 'commit_edit.php?fn=".$new_fname."&ln=".$new_lname."&e=".$new_email."&u=".$new_username."&p=&ec=".$email_changed."';
								</script>
							";
						} else if ($new_picture!==null) {
							echo "	
								<script>
									window.location = 'commit_edit.php?fn=".$new_fname."&ln=".$new_lname."&e=".$new_email."&u=".$new_username."&p=".$new_picture."&ec=".$email_changed."';
								</script>
							";
						}
					} else {
						echo "
							<script> 
								alert ('Invalid Password. Profile information not updated.'); 
								window.location = 'home.php';
							</script>
						";
					}
				} else {
					echo "
						<script> 
							alert ('Validation Error');
							window.location = 'edit_profile.php?fn=$fname_valid&ln=$lname_valid&e=$email_valid&u=$user_valid&ec=$email_changed';
						</script>
					";
				}


				//header('Location: commit_edit.php?fn=$new_fname&ln=$new_lname&e=$new_email&u=$new_username&p=$new_picture');

				
			}
		?>


	</div>

</body>
</html>

