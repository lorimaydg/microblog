<?php

	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}

	include ('db.php');

	$id_logged = $_SESSION['id_logged'];
	$uid = $_GET['u'];
	$pid = $_GET['p'];
	$i = $_GET['i'];
	$page = $_GET['page'];

	$sql = $con->prepare("DELETE FROM reposts WHERE user_id=? AND post_id=?");
	$sql->bind_param("ii", $id_logged, $pid);		
	$sql->execute();

	if ($i==1) {
		header("Location: home.php?page=$page");
	} else if ($i==2) {
		header("Location: view_user_profiles.php?id=$uid");
	} else if ($i==3) {
		header("Location: all_post.php");
	} else if ($i==4) {
		header("Location: view_post.php?p=$pid");
	} 

	$sql->close();
	$con->close();
	
?>