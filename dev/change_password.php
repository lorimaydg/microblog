<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
	<script src="../lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
	<script src="../lib/JavaScript-MD5-master/js/md5.min.js"></script>
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar-disabled.php');

	?>

	<div class="main">

		<br/>
		<div align="center">
			<label class="edit_profile">CHANGE PASSWORD</label>
		</div>
		<br/><br/>
		<form method="POST" action="">

			<br/>
			<label class="edit_labels">CURRENT PASSWORD:</label>
			<input class="edit_fields" style="left: 90px;" type="password" name="current_password" id="current_password" />
			<label style="color:red; position:relative; left:107px;" id="error_curr"></label><br/>

			<label class="edit_labels">NEW PASSWORD:</label>
			<input class="edit_fields" style="left: 123px;" type="password" name="new_password" id="new_password" />
			<label style="color:red; position:relative; left:140px;" id="error_new"></label><br/>

			<label class="edit_labels">RETYPE NEW PASSWORD:</label>
			<input class="edit_fields" style="left: 66px;" type="password" name="new_password2" id="new_password2" />

			<br/><br/><br/><br/>
			<input class="edit_save" style="position:relative; left:600px;" type="submit" name="save" id="save" value="SAVE CHANGES">

		</form>
			
		<a href="edit_profile.php"><button class="edit_cancel" style="position:relative; left:810px;">CANCEL</button></a><br/>

	</div>

	<?php

		if (isset($_POST['save'])) {

			$current = $_POST['current_password'];
			$new = $_POST['new_password'];
			$new2 = $_POST['new_password2'];

			$old_pass_valid = 0;
			$new_pass_valid = 0;


			// PASSWORD VALIDATION //

				if ($current==null) : ?>
			       	<script type='text/javascript'>
			        	document.getElementById('error_curr').innerHTML = '* Input Current Password';
			        </script>

				<?php elseif (MD5($current)!==$password) : ?>
			       		<script type='text/javascript'>
			        		document.getElementById('error_curr').innerHTML = '* Wrong Password';
			        	</script>
				<?php else :
					$old_pass_valid = 1;
				endif; 

				if ($new == null) : ?>
				    <script type='text/javascript'>
						document.getElementById('error_new').innerHTML = '* Passwords are required';
					</script>
					
			    <?php elseif ($new2 == null) : ?>
				    <script type='text/javascript'>
						document.getElementById('error_new').innerHTML = '* Passwords are required';
					</script>
				<?php else : 
					if ($new == $new2) : 
						$new_pass_valid = 1;
					else : ?>
				       	<script type='text/javascript'>
				       		document.getElementById('error_new').innerHTML = '* Passwords do not match';
				       	</script>
				<?php
					endif;
				endif;
				

			// END OF PASSWORD VALIDATION //

			if ($old_pass_valid==1 && $new_pass_valid==1) {
				$sql = $con->prepare("UPDATE users SET password=MD5(?) WHERE id=?");
				$sql->bind_param("si", $new, $id_logged);	
				$sql->execute(); ?>

				<script type='text/javascript'>
					alert('Successfully changed password. Please log in.');
					location.href = 'check_logout.php';
				</script>
				
				<?php
				
			}

		}

	?>

</body>
</html>

