<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

	?>

	<div class="main">
		
		<?php

			$acc_viewed = $_GET['id'];

			$sql7 = "SELECT * FROM users WHERE id='$acc_viewed'";
		    $result7 = $con->query($sql7);


	        while ($row7 = mysqli_fetch_assoc($result7)) {
	        	$a_name = $row7['firstname']." ".$row7['lastname'];
	        	$a_username = $row7['username'];
	        	$a_img = $row7['image'];
	        	$a_bday = $row7['birthday'];
	        	$a_gender = $row7['gender'];
	        	$a_joined = $row7['created'];
	        }

	        $sql8 = "SELECT count(*) AS a_num_posts FROM posts WHERE user_id='$acc_viewed'";
			$result8 = $con->query($sql8);

	        while ($row8 = mysqli_fetch_assoc($result8)) {
	        	$a_num_posts = $row8['a_num_posts'];
	        }

	        $sql10 = "SELECT count(*) AS a_num_followers FROM followers WHERE follower_id='$acc_viewed'";
			$result10 = $con->query($sql10);

	        while ($row10 = mysqli_fetch_assoc($result10)) {
	        	$a_num_followers = $row10['a_num_followers'];
	        }

			$sql11 = "SELECT count(*) AS a_num_following FROM followers WHERE user_id='$acc_viewed'";
			$result11 = $con->query($sql11);

	        while ($row11 = mysqli_fetch_assoc($result11)) {
	        	$a_num_following = $row11['a_num_following'];
	        }


	        $a_bday_year = substr($a_bday,0,4);
	        $a_bday_month = substr($a_bday,5,2);
	        $a_bday_day = substr($a_bday,8,2);

	        $a_joined_year = substr($a_joined,0,4);
	        $a_joined_month = substr($a_joined,5,2);
	        $a_joined_day = substr($a_joined,8,2);

			if ($a_gender==1) {
	        	$a_genderrr = 'Male';
	        }
	        if ($a_gender==2) {
	        	$a_genderrr = 'Female';
	        }


	        $acc_to_follow = $_GET['id'];

			$sql9 = "SELECT * FROM followers WHERE user_id='$id_logged'";
			$result9 = $con->query($sql9);

			$you_follow = 0;

			while ($row9 = mysqli_fetch_assoc($result9)) {
				if ($row9['follower_id'] == $acc_to_follow) {
					$you_follow = 1;
				}
			}

			$sql12 = "SELECT * FROM followers WHERE follower_id='$id_logged'";
			$result12 = $con->query($sql12);

			$follows_you = 0;

			while ($row12 = mysqli_fetch_assoc($result12)) {
				if ($row12['user_id'] == $acc_to_follow) {
					$follows_you = 1;
				}
			}

		?>

		<div class="container-left" align="center">
			<img src='../img/users/<?php echo $a_img ?>' class="display-view-div-img" />
		</div>
		<div class="container-center" align="center">
			<label class='display-view-div-label'><?php echo $a_name ?></label><br/><br/>
			<div>
				<label class='display-view-div-text'><?php echo $a_username; ?>
				<?php
					if ($follows_you==1) {
						echo "<div style='color:white;background:gray;font-size:10px;width:75px;'>Follows You</div>";
					} else {
						echo "<br/>";
					}
				?>
				</label>
			</div><br/>
			<div class="posts-view-div">
					<label style="font-size: 19px; font-weight: bold;"><?php echo $a_num_posts ?></label><br/>
					<label>POSTS</label>
			</div>
			<div class="following-view-div">
					<label style="font-size: 19px; font-weight: bold;"><?php echo $a_num_following ?></label><br/>
					<label>FOLLOWING</label>
			</div>
			<div class="follower-view-div">
					<label style="font-size: 19px; font-weight: bold;"><?php echo $a_num_followers ?></label><br/>
					<label>FOLLOWERS</label>
			</div>
		</div>
		<div class="container-right" align="center">
			<div class="view-right">
				<br/>
				<label>BIRTHDAY: &nbsp;</label>
				<text><?php echo $m[$a_bday_month-1]." ".$a_bday_day.", ".$a_bday_year ?></text><br/>
				<label>GENDER: &nbsp;</label>
				<text><?php echo $a_genderrr ?></text><br/>
				<label>JOINED: &nbsp;</label>
				<text><?php echo $m[$a_joined_month-1]." ".$a_joined_day.", ".$a_joined_year ?></text><br/>
			</div>
			<?php

				$_SESSION['follow'] = $acc_to_follow;

				if ($you_follow == 1) {
					echo "<a href='unfollow_users.php?' class='display-view-div-btn' style='text-decoration:none; color:white; padding:5px 10px; background:#0099cc; border: 1px solid #0099cc'>Unfollow</a>";
				} else {
					echo "<a href='follow_users.php?' class='display-view-div-btn' style='text-decoration:none; color:black; padding:5px 10px;'>Follow</a>";
				}

			?>
			
		</div><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

		<?php

			$acc_viewed = $_GET['id'];

			$no_posts = 0;

			$sql6 = "SELECT users.id AS uid, users.username, users.image, posts.id AS pid, posts.content, date(posts.date_posted) AS date_posted, time(posts.date_posted) AS time_posted FROM users INNER JOIN posts ON posts.user_id = users.id WHERE user_id='$acc_viewed' ORDER BY posts.date_posted DESC";
			$result6 = $con->query($sql6);

		    while ($row6 = mysqli_fetch_assoc($result6)) {

		    	$pid = $row6['pid'];

		    	//counting likes
		    	$sql13 = "SELECT count(*) AS count_likes FROM likes WHERE post_id = '$pid'";
				$result13 = $con->query($sql13);
		    	$row13 = mysqli_fetch_assoc($result13);

		    	//counting reposts
		    	$sql14 = "SELECT count(*) AS count_reposts FROM reposts WHERE post_id = '$pid'";
				$result14 = $con->query($sql14);
		    	$row14 = mysqli_fetch_assoc($result14);

		    	//if liked or not
				$sql15 = "SELECT likes.user_id AS luid, likes.post_id AS lpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM likes INNER JOIN posts ON likes.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result15 = $con->query($sql15);

				$you_liked = 0;

				while ($row15 = mysqli_fetch_assoc($result15)) {
					if ($row15['luid']==$id_logged && $row15['lpid']==$row6['pid'] && $row15['puid']==$row6['uid']) {
						$you_liked = 1;
					}
				}

				//if reposted or not
				$sql16 = "SELECT reposts.user_id AS ruid, reposts.post_id AS rpid, posts.id AS pid, posts.user_id AS puid, posts.content AS content, posts.date_posted AS posted, users.id AS uid, users.username AS username FROM reposts INNER JOIN posts ON reposts.post_id=posts.id INNER JOIN users ON posts.user_id=users.id";
				$result16 = $con->query($sql16);

				$you_reposted = 0;

				while ($row16 = mysqli_fetch_assoc($result16)) {
					if ($row16['ruid']==$id_logged && $row16['rpid']==$row6['pid'] && $row16['puid']==$row6['uid']) {
						$you_reposted = 1;
					}
				}

		       	$posted_date = explode ("-", $row6['date_posted']);
        		$posted_time = explode (":", $row6['time_posted']);

		        if ($posted_time[0]>=0 && $posted_time[0]<=11) {
		        	$ampm = 'am';
		        } else if ($posted_time[0]>=12 && $posted_time[0]<=23) {
		        	$ampm = 'pm';
		        }

		      	?>
			       	<div class='display-view-post-div'>
			       		<div>
					   		<img src='../img/users/<?php echo htmlspecialchars($row6['image']) ?>' class='display-post-div-image' />
					   	</div>
			        	<label class='display-post-div-label'><?php echo htmlspecialchars($row6['username']) ?></label>
			        	<label class='display-post-div-date'><?php echo htmlspecialchars($m[$posted_date[1]-1])." ".htmlspecialchars($posted_date[2]).", ".htmlspecialchars($posted_date[2])." ".htmlspecialchars($posted_time[0]).":".htmlspecialchars($posted_time[1])." ".htmlspecialchars($ampm) ?></label><br/>
						<div>
							<text class='display-post-div-text'><?php echo htmlspecialchars($row6['content']) ?></text><br/><br/>
						</div>
						<div class='like_repost_edit_delete'>

					<?php	if ($you_liked==1) : ?>
								<form method="POST" action="unlike_post.php?i=2">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unlike" />
										</a>
									</label> &nbsp;&nbsp;
					<?php	else : ?>
								<form method="POST" action="like_post.php?i=2">
									<label id='like_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='like_btn'><?php echo $row13['count_likes'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action" id="action" class='like-btn-click' value="Like" />
										</a>
									</label> &nbsp;&nbsp;
					<?php 	endif;

						if ($you_reposted==1) : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row14['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' style='color:white;background:#0099cc;border:1px solid #0099cc' value="Unrepost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php	else : ?>
									<label id='repost_text'>
										<a href='' class='display-post-div-btn'>
											<label style='color:black;font-size:14px;' id='repost_btn'><?php echo $row14['count_reposts'] ?>&nbsp;&nbsp;&nbsp;</label>
											<input type="hidden" name="uid2" value="<?php echo $row6['uid'] ?>">
											<input type="hidden" name="pid2" value="<?php echo $row6['pid'] ?>">
											<input type="submit" name="action2" id="action2" class='like-btn-click' value="Repost" />
										</a>
									</label> &nbsp;&nbsp;
								</form>
					<?php endif; 

				echo "	</div>
					</div>
				";

				$no_posts = 1;
		    }

		    if ($no_posts == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Posts</label>";
		    }

			$con->close();

		?>

		<br/>
	</div>

</body>
</html>