
	<div class="header">
		<br/><h1 class="header-text">MicroBlog</h1>
		<a href="home.php"><button class="menu-btn" disabled>HOME</button></a>
		<a href="follow_tab_notif.php"><button class="menu-btn" disabled>NOTIFICATIONS</button></a>

		<form method="POST" action="search_page.php">
			<input class="search-input" type="text" name="keyword" id="keyword" placeholder="SEARCH"  disabled />
			<input class="search-btn" type="submit" name="search" id="search" value="SEARCH" disabled />
		</form>
	</div>

	<?php
        
		include ('db.php');

	    $sql = $con->prepare("SELECT * FROM users WHERE id=?");
		$sql->bind_param("i", $id_logged);
		$sql->execute();

		$result = $sql->get_result();
		while($row = $result->fetch_assoc()) {
        	$id = $row['id'];
        	$prof_img = $row['image'];
        	$name = $row['firstname']." ".$row['lastname'];
        	$username = $row['username'];
        	$gender = $row['gender'];
        	$bday = date($row['birthday']);
        	$joined = date($row['created']);
        	$activation = $row['activation'];
        	$firstname = $row['firstname'];
        	$lastname = $row['lastname'];
        	$email = $row['email'];
        	$password = $row['password'];
        }

        $birth = explode ("-", $bday);
        $join = explode ("-", $joined);

        $m = array ("January","February","March","April","May","June","July","August","September","October","November","December");

        #GET GENDER

        if ($gender==1) {
        	$genderrr = 'Male';
        }
        if ($gender==2) {
        	$genderrr = 'Female';
        }

        date_default_timezone_set('Asia/Manila');
        $current_date_time = date('Y-m-d G:i:s');


		#GETTING NUMBER OF POSTS

		$sql3 = $con->prepare("SELECT count(*) AS num_posts FROM posts WHERE user_id=?");
		$sql3->bind_param("i", $id_logged);
		$sql3->execute();

		$result3 = $sql3->get_result();
		while($row3 = $result3->fetch_assoc()) {
        	$num_posts = $row3['num_posts'];
        }

        #GETTING NUMBER OF FOLLOWERS

		$sql4 = $con->prepare("SELECT count(*) AS num_followers FROM followers WHERE follower_id=?");
		$sql4->bind_param("i", $id_logged);
		$sql4->execute();

		$result4 = $sql4->get_result();
		while($row4 = $result4->fetch_assoc()) {
        	$num_followers = $row4['num_followers'];
        }

        #GETTING NUMBER OF FOLLOWING

		$sql5 = $con->prepare("SELECT count(*) AS num_following FROM followers WHERE user_id=?");
		$sql5->bind_param("i", $id_logged);
		$sql5->execute();

		$result5 = $sql5->get_result();
		while($row5 = $result5->fetch_assoc()) {
        	$num_following = $row5['num_following'];
        }

	?>

	<div class="sidebar">
		<div class="image_div">
			<img src="<?php echo '../img/users/'.$prof_img ?>" height="130px" width="130px" style="position: relative; left: 5px; top: 5px" />
		</div><br/><br/><br/>

		<div align="center">
			<label class="text_name"><?php echo $name ?></label><br/>
			<label class="text_username"><?php echo $username ?></label>
		</div>

		<div align="center">
			<a href="all_post.php" class="href-post-wing-wers">
				<div class="post-div">
					<label class="label_cursor" style="font-size: 19px; font-weight: bold;"><?php echo $num_posts ?></label><br/>
					<label>POSTS</label>
				</div>
			</a>
			<a href="all_following.php" class="href-post-wing-wers">
				<div class="following-div">
					<label class="label_cursor" style="font-size: 19px; font-weight: bold;"><?php echo $num_following ?></label><br/>
					<label>FOLLOWING</label>
				</div>
			</a>
			<a href="all_followers.php" class="href-post-wing-wers">
				<div class="follower-div">
					<label class="label_cursor" style="font-size: 19px; font-weight: bold;"><?php echo $num_followers ?></label><br/>
					<label>FOLLOWERS</label>
				</div>
			</a>
		</div>

		<hr style="color: white; border: 1px solid white;" width="250px">

		<div class="details_div">
			<label style="font-weight: bold; font-size: 19px; position: relative; left: 50px;">DETAILS:</label><br/>
			<label>BIRTHDAY: &nbsp;&nbsp;&nbsp;</label>
			<text><?php echo $m[$birth[1]-1]." ".$birth[2].", ".$birth[0] ?></text><br/>
			<label>GENDER: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<text><?php echo $genderrr ?></text><br/>
			<label>JOINED: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<text><?php echo $m[$join[1]-1]." ".$join[2].", ".$join[0] ?></text><br/>
		</div>

		<br/>
		<a href="edit_profile.php"><button class="bottom-btn" disabled disabled>EDIT PROFILE</button></a>
		<a href="check_logout.php"><button class="bottom-btn" disabled>LOGOUT</button></a>
	</div>
