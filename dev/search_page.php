<?php
	session_start();

	if (!isset($_SESSION['id_logged'])) {
		header('Location: ../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MicroBlog</title>
	<link rel="stylesheet" href="../css/Style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline|Cairo|Coustard|Leckerli+One|Pacifico" rel="stylesheet">
</head>
<body>

	<?php

		$id_logged = $_SESSION['id_logged'];
		include ('header-sidebar.php');

	?>

	<div class="main">
		
		<?php 

		if (isset($_POST['search'])) {

			echo "<label class='search_results_txt'>Search Results for: ".htmlspecialchars($_POST['keyword'])."</label><br/><br/><br/><br/>";

			$keyword = htmlspecialchars($_POST['keyword']);
			$no_search_results = 0;

			$sql2 = "SELECT * FROM users WHERE username LIKE '%$keyword%' OR firstname LIKE '%$keyword%' OR lastname LIKE '%$keyword%'";
			$result2 = $con->query($sql2);

		    while ($row2 = mysqli_fetch_assoc($result2)) {
		    	if ($row2['id']!==$id_logged) {
			    	echo "
				       	<div class='display-search-div'>
				       		<img src='../img/users/".htmlspecialchars($row2['image'])."' class='display-search-div-img' />
				       		<label class='display-search-div-label'>".htmlspecialchars($row2['firstname'])." ".htmlspecialchars($row2['lastname'])."</label><br/>
				       		<label class='display-search-div-text'>".htmlspecialchars($row2['username'])."</label><br/>
				       		<a href='view_user_profiles.php?id=".$row2['id']."'><button class='display-search-div-btn'>View Profile</button></a>
						</div>
					";

					$no_search_results = 1;
				}
		    }

		    if ($no_search_results == 0) {
		    	echo "<label class='search_results_txt' style='font-size: 20px'>No Results!</label>";
		    }
		} 

		?>

		<br/>
	</div>

</body>
</html>